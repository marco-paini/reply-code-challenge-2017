package handsome.code.monkeys.data;

import lombok.Data;

@Data
public class Event {
	public long endTime;

	public Integer member;

	public String name;

	public long statTime;

	public Event(final String name, final long statTime, final long endTime, final Integer member) {
		this.name = name;
		this.statTime = statTime;
		this.endTime = endTime;
		this.member = member;
		if (member > 100) {
			throw new RuntimeException("Event capacity greater than 100");
		}
	}
}
