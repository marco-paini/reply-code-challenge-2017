package handsome.code.monkeys.data;

import java.util.ArrayList;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class MyList extends ArrayList<Event> {
	private static final long serialVersionUID = -3940946768833541430L;

	@Override
	public boolean add(final Event e) {
		if (this.size() == 0) {
			return super.add(e);
		}
		for (int i = 0; i < this.size(); i++) {
			Event e1 = this.get(i);
			boolean a = e.statTime > e1.statTime && e.statTime < e1.endTime;
			boolean b = e.endTime > e1.statTime && e.endTime < e1.endTime;
			boolean c = e.statTime == e1.statTime && e.endTime == e1.endTime;
			boolean d = e.statTime < e1.statTime && e.endTime > e1.endTime;
			if (a || b || c || d) {
				return false;
			}
		}
		for (int i = 0; i < this.size(); i++) {
			if (i == 0 && this.get(i).statTime >= e.endTime) {
				super.add(0, e);
				return true;
			}
			if (i == (this.size() - 1)) {
				if (this.get(this.size() - 1).endTime <= e.statTime) {
					super.add(e);
					return true;
				}
				return false;
			}
			if (compatibile(e, this.get(i), this.get(i + 1))) {
				super.add(i + 1, e);
				return true;
			}
		}
		return false;
	}

	private boolean compatibile(Event candidato, Event prima, Event dopo) {
		return prima.endTime <= candidato.statTime && dopo.statTime >= candidato.endTime;
	}
}
