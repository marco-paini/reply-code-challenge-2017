package handsome.code.monkeys.data;

import lombok.Data;

@Data
public class Room {
	public Integer capacity;

	public String name;

	public Room(final String name, final Integer capacity) {
		this.name = name;
		this.capacity = capacity;
		if (capacity > 100) {
			throw new RuntimeException("Room capacity greater than 100");
		}
	}
}
