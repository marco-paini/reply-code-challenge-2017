package handsome.code.monkeys;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Stream;

import handsome.code.monkeys.data.Event;
import handsome.code.monkeys.data.MyList;
import handsome.code.monkeys.data.Room;
import javafx.util.Pair;
import lombok.extern.jbosslog.JBossLog;

@JBossLog
public class HandsomeCodeMonkeys {
	private class MyComparator implements Comparator<Float> {
		@Override
		public int compare(Float o1, Float o2) {
			return (int) (o2 - o1);
		}
	}

	private int E = 0;

	private final ArrayList<Event> events = new ArrayList<>();

	private int line = 0;

	private final String output = System.getProperty("user.dir") + "/../src/main/resources/output/";

	private final ArrayList<Room> rooms = new ArrayList<>();

	public HandsomeCodeMonkeys(Path file) {
		log.info("=== " + file + " ===");
		try (Stream<String> stream = Files.lines(file)) {
			/* INPUT */
			stream.forEach(string -> {
				String[] strings = string.split(" ");
				if (line == 0) {
					E = Integer.valueOf(strings[0]);
				} else if (line > 0 && line <= E) {
					events.add(new Event(strings[0], Long.valueOf(strings[1]), Long.valueOf(strings[2]),
							Integer.valueOf(strings[3])));
				} else {
					rooms.add(new Room(strings[0], Integer.valueOf(strings[1])));
				}
				line++;
			});
			/* INPUT */

			/* CODE */
			Map<Float, List<Pair<Event, Room>>> map = new TreeMap<>(new MyComparator());
			events.forEach(e -> {
				rooms.forEach(r -> {
					if (r.capacity <= e.member) {
						return;
					}
					Float score = getScore(e, r);
					if (map.containsKey(score)) {
						map.get(score).add(new Pair<Event, Room>(e, r));
						return;
					}
					List<Pair<Event, Room>> l = new ArrayList<>();
					l.add(new Pair<Event, Room>(e, r));
					map.put(score, l);
				});
			});
			Map<Room, MyList> res = new HashMap<>();
			Set<Event> eventAdded = new HashSet<>();
			map.entrySet().forEach(entry -> {
				entry.getValue().forEach(pair -> {
					Room value2 = pair.getValue();
					if (eventAdded.contains(pair.getKey())) {
						return;
					}
					if (res.containsKey(value2)) {
						Event event = pair.getKey();
						if (res.get(pair.getValue()).add(event)) {
							eventAdded.add(event);
						}
					} else {
						MyList e = new MyList();
						if (e.add(pair.getKey())) {
							eventAdded.add(pair.getKey());
						}
						res.put(value2, e);
					}
				});
			});
			/* CODE */

			/* OUTPUT */
			final StringBuilder stringBuilder = new StringBuilder();
			rooms.forEach(room -> {
				stringBuilder.append(room.name + ":");
				if (res.containsKey(room)) {
					res.get(room).forEach(event -> stringBuilder.append(event.name + " "));
				}
				stringBuilder.append("\n");
			});
			Files.write(Paths.get(output + file.getFileName()), stringBuilder.toString().getBytes());
			log.info(stringBuilder);
			/* OUTPUT */
		} catch (IOException e) {
			log.error(e);
		}
	}

	public Float getScore(Event e, Room r) {
		return (Float.valueOf(e.member) / Float.valueOf(r.capacity)) * Float.valueOf((e.endTime - e.statTime));
	}
}
