package handsome.code.monkeys;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import lombok.SneakyThrows;

@QuarkusMain
public class Main implements QuarkusApplication {
	@ConfigProperty(name = "limit", defaultValue = "4")
	long limit;

	@ConfigProperty(name = "skip", defaultValue = "0")
	long skip;

	private String input(final String string) {
		return "/input/" + string;
	}

	@Override
	public int run(String... args) throws Exception {
		Stream.of("data_5000_3.in", "data_5000_10.in", "data_50000_10.in", "data_50000_100.in").skip(skip).limit(limit)
				.map(this::input).map(getClass().getClassLoader()::getResource).map(URL::getPath).map(Paths::get)
				.forEach(this::write);
		return 0;
	}

	@SneakyThrows
	private void write(final Path path) {
		new HandsomeCodeMonkeys(path);
		new Result(path);
	}
}
